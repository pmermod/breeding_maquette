\documentclass[a4paper,12pt]{article}
\usepackage{lastpage}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{array}
\usepackage{fancyhdr}
\usepackage{verbatim}
\usepackage{framed}
\usepackage[top=3cm,bottom=2.5cm]{geometry}
%\usepackage[headheight=110pt, bottom=1.5cm,left=2cm,right=2cm, a4paper]{geometry}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage{pdfpages}
\linespread{1.05} 
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\begin{document}

\sloppy
%\linespread{1.5}

\thispagestyle{empty}

\begin{center}

\includegraphics[width=0.7\linewidth]{./fig/studio_clouzo_logo.png}

\vspace{5cm}

\textbf{
\huge
Game Design Document} \\
%Breeding \\
\includegraphics[width=0.8\linewidth]{./fig/breeding_logo.png} \\

\vspace{0.2cm}

\large
Breed the largest monkey family!

\vspace{8cm}

All work Copyright ©2018 by Studio Clouzo

Written by Philippe Mermod

Version 2.1

Monday, February 19, 2018

\end{center}

\newpage

%\tableofcontents

\normalsize

\section{Design history}

\begin{itemize}
\item[v1.1] First prototype, December 2015: The first version of the game mechanics was developed by Philippe Mermod -- implemented in C++ and playable in text mode. 
\item[v1.2] Pilot, September 2016: the user graphical interface was implemented in Unity by Rémi La Marra, Pierre-Igor Berthet and Boris Poget, using the game mechanics of version 1.1 -- playable on most platforms, successfully tested in several gaming events, download available. 
\item[v2.1] Second prototype, March 2017: New features were added to increase the strategical possibilities and achieve a more fun and interesting player experience -- implemented in C++ by Philippe Mermod and playable in text mode. 
\item[v2.2] First release, scheduled for December 2018: the pilot needs to be upgraded to the game mechanics of version 2.1, as well as tested and finalised -- in development.
\end{itemize}

\section{Game philosophy}

\subsection{Board game 2.0}

The founding concept of the Studio Clouzo company is that of ``board game 2.0'', i.e, turn-based video games designed for several players in the same room, in which the main emphasis is on stimulating exchanges between players. The machine offers clear advantages as compared with regular board games, such as complex in-game calculations to update player status or handle complicated random outcomes, automatic score counting, artificial intelligence (AI), animations, ambience sounds and musics, and game saves. The idea of exploiting such possibilities to create a unique shared experience for people playing together opens a lot of room for innovation. Breeding is the first game to be designed specifically to follow this pioneering concept.

\subsection{Breeding}

Breeding is, broadly speaking, about life in its most basic sense: reproduction and survival. Reproduction lies at its core, as each player governs a monkey whose goal is to produce the largest possible descendance through mating. Interesting situations arise right from the start as a player has no other choice than to mate his or her monkey to those belonging to other players, with the resulting babies giving an equal amount of points to both parents. As the game develops, the situation becomes more complicated as the original monkeys become grand-parents, uncles and aunts, etc. The often hilarious ambiguities that continuously arise, as well as problematics such as paternity and male/female specific strategies, create a setting in which players will automatically engage in heated negotiations and supplement the game with jokes of their own making. By design, the rules of breeding are simple and encourage the players themselves to create a unique and memorable experience.  

\section{Core game play mechanics}

\subsection{Monkeys and tribes}

Breeding is a turn-based game for 4 players, with 1 to 4 human players (the reminder being played by the AI). Each player governs a monkey, who is also the chief of a family. Each family, or tribe, consists of all living monkeys sharing at least 50\% of the chief's genes -- at the start of the game, this comprises only the chief (governed by the player) and a brother or sister (governed by the AI, but which can, as all other tribe members, be influenced by the player). The number of monkeys grows as more babies are born. Each tribe also has a dedicated reserve of bananas. The monkeys have an energy gauge and die if it gets to zero; when they get older, some amount of energy is lost each turn. 

At the beginning of the game, each player chooses a name, sex, and feature gene for its chief monkey. Feature genes provide a specific advantage and can be passed over to offspring. There is a choice of five of them: nurse (increases the energy given while loving), warrior (increases the energy removed while striking), charm (increases the chance to be loved by a monkey governed by the AI), tenacious (decreases the effect of ageing), and agile (increases the yield while searching).

\subsection{Base actions}

Each turn, each monkey, including those governed by the AI, can chose between three base actions: love, strike, and search. The love and strike actions are performed on another monkey and cost bananas, and the search action provides bananas and, when performed by a chief, has some chance of providing an object. 

The action of loving or striking gives or removes energy to the target monkey, respectively. Loving can be performed by a monkey on itself to heal. If the action of loving is performed on a monkey who is not of the same tribe and of the opposite sex, the action of reciprocating the love by the other monkey results in mating between the two monkeys instead of providing energy. The female monkey can then get pregnant, with a probability which depends on the number of matings and the number of turns since the first mating. A pregnant female passes her turn, and the next turn gives birth to a baby with no energy. The baby inherits half its genes from the father, who is chosen randomly among the males who previously mated with the mother. The baby cannot perform actions and cannot be hit, but can (and needs to) be loved. After two turns, the baby becomes an adult monkey who joins the corresponding tribe and can perform actions just as the other monkeys.   

A chief monkey who possesses at least one object can also perform a fourth action on any monkey instead of the three actions mentioned above: use an object. A variety of different objects exists with different effects, such as providing or removing a large amount of energy, forcing an action from the target monkey, ageing or rejuvenating, spoiling bananas, protecting against attacks, and causing a female to become pregnant, fertilised, non-fertilised, or to get twins.  

\subsection{Totems}

Chiefs also have control over a so-called totem which can be enabled or disabled at will while playing their turns. A totem will force the monkeys from the chief's tribe to perform a certain action, if they can: love with the love totem, strike with the war totem, search with the exploration totem. If no totem is enabled, the decision is left to the AI. 

\subsection{Trade}

During the turn, a player has an unlimited possibility to trade with other players. Items which can be exchanged are bananas and objects, and the amount is negotiated and decided by the players. 

\subsection{Events}

Random events which affect the whole monkey village take place at a few moments during the course of the game. These often force the players to adapt their strategies in unpredictable ways. Examples of event effects are: removing energy from all adult monkeys, giving energy to all babies, providing an additional object to all chiefs, reducing or increasing the banana yields while searching during one turn, or forcing the love or war totems for all tribes during one turn.  

\subsection{End of game}

After a determined number of turns corresponding roughly to a 20$-$30 min duration, the game ends and the winner is the player with the most points. The points are counted as follows: each time a baby is born, it provides points proportional to the fraction of genes it carries belonging to the player's tribe; then, at the end of the game, each living adult offspring again provides the same amount of points.

\section{User interface}

\subsection{Platforms}

Breeding can be played on a PC with most operating systems, on a tablet computer, or on a game console. It will be first released for PC to ensure no compatibility issues. At first release, there will be a choice of three languages: English, French, or German. 

\subsection{Player view and actions}

The game takes advantage of Unity's 3D engine and provides two possible views: a view centred on any monkey with a button for switching between them; or a global view of the monkey village in which a player can click to select a monkey. When the view is on a monkey, there are buttons for the valid base actions which can be performed. There is a special switch for the totems, and a specific dialog for trading. 

\subsection{Graphics and animations}

Male and female monkeys look different, and colours are used to distinguish between tribes. Each tribe possesses a hut and a banana reserve by a fire, with the female monkeys from this tribe tending to cluster around the hut and the males around the fire. Babies stay next to their mothers until they grow up. A monkey performing the love action goes to the target monkey and waves its arms in a healing manner, and the energy given appears in green. A monkey performing the strike action goes to the target monkey and hits it on the face. Whenever a mating occurs, the pair goes into the hut corresponding to the female's tribe and hearts appear above the hut. 

Specific graphics and/or animations are used at given points in the game, such as when a female gets pregnant, when a monkey is born or dies, when an object is used, or when an event occurs. At the end of the game, a specific animation looping on each monkey is used for point counting, followed by a scene with a podium to celebrate the winner. 

\subsection{Sounds and music}

A music theme welcomes the player during the initial menu and choice of monkey characteristics. Specific sounds are used for various occasions such as loving, hitting, dying, etc. Ambience sounds are present during the game, such as fire burning and waves on the beach, insects, etc.   


\end{document}



