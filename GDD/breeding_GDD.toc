\contentsline {section}{\numberline {1}Design history}{2}
\contentsline {section}{\numberline {2}Game philosophy}{2}
\contentsline {subsection}{\numberline {2.1}Board game 2.0}{2}
\contentsline {subsection}{\numberline {2.2}Breeding}{3}
\contentsline {section}{\numberline {3}Core gameplay mechanics}{3}
\contentsline {subsection}{\numberline {3.1}Monkeys and tribes}{3}
\contentsline {subsection}{\numberline {3.2}Base actions}{4}
\contentsline {subsection}{\numberline {3.3}Totems}{4}
\contentsline {subsection}{\numberline {3.4}Exchanges}{4}
\contentsline {subsection}{\numberline {3.5}Events}{4}
\contentsline {subsection}{\numberline {3.6}End of the game}{5}
\contentsline {section}{\numberline {4}Game interface}{5}
