using namespace std;

#include "breeding_names.h"

//////////////////////////////////////////////////

class Clan {
public:
    int number;
    string name;
    int bananas;
    int totem;      // 0=no totem; 1=exploration; 2=love; 3=war; 
    int scorebabies;   // permanent score when babies are born
    int scoreadults;   // extra score from living adults when the game ends
    void print();
};   

void Clan::print() {
  if(number==0) cout << yellowbold; 
  if(number==1) cout << greenbold; 
  if(number==2) cout << cyanbold; 
  if(number==3) cout << magentabold; 
  cout << name;
  cout << reset << " - bananas " << yellowbold << bananas;
  cout << reset << " - score " << whitebold << scorebabies+scoreadults << reset << " (" << scorebabies << "+" << scoreadults << ")";
  if(totem==1) cout << blue << " - exploration totem";
  if(totem==2) cout << magenta << " - love totem";
  if(totem==3) cout << red << " - war totem";  
  if(totem==4) cout << green << " - healing totem";    
  cout << reset << endl;
}  

class Creature {
public:
    int generation;
    float age;
    float energy;
    int chief;     // -1 if not chief otherwise clan number
    vector<int> isinclan;   // clan numbers 
    int isai;      // 0 or 1
    int isfemale;  // 0 or 1
    int isadult;   // 0 or 1
    int isold;     // 0 or 1
    int isdead;    // 0 or 1
    int pregnant;  // -1 if pregnant otherwise pregnancy level
    string enchanted;  // "no" otherwise forces to love creature with this name
    int ispoisoned;    // number of turns 
    int ispotioned;    // number of turns 
    int isarmoured;    // number of hits 
    int extraturn;    // number of extra turns 
    bool cupandballed;  // if true, forces to attack oneself
    bool willhavetwins;
    int objectprob; // object finding level
    int nameindex;
    string name;
    string featuregene[5][2];  /// order: mane, claws, pocket, longevity, climber
    string feature[5];
    vector<string> objects;
    string myaction_who;
    string myaction_what;
    int myaction_nturns;
    Names setname(Names,int);
    vector<Creature> eggs; 
    void kill();
    Creature conceive(Creature);
    Creature deliver();
    float thisturncost(bool);
    bool isclan(int);
    bool sameclan(Creature);
    vector<int> gettotems(vector<Clan>);
    int dispo(vector<Clan>);
    int cost(vector<Clan>,int,string,string);
    int hasobject(string);   /// -1 if no object otherwise object index
    void loseobject(string);
    void printname();
    void print();
    bool oppositesex(Creature);
    vector<float> heritage;
    vector<string> history_children;
    vector<string> history_carers;
    vector<string> history_harassers;
    vector<string> history_lovers;
    float child_care;
    float child_care_by_males;
    float child_harassment;
    int aiwhatwithwhom(vector<Creature>,vector<Clan>,string,string);
    int aichoosetotem();
};    

Creature Creature::conceive(Creature father) {
  Creature newegg;
  newegg.generation=max(generation+1,father.generation+1);
  /// all eggs are ai
  newegg.chief=-1;  
  newegg.isai=1;
  /// sex is random
  newegg.isfemale=rand()%2;
  /// child at start
  newegg.isadult=0;
  newegg.isold=0;
  newegg.isdead=0;
  newegg.pregnant=0;
  newegg.enchanted="no";
  newegg.ispoisoned=0;
  newegg.ispotioned=0;
  newegg.isarmoured=0;
  newegg.extraturn=0;
  newegg.cupandballed=0;
  newegg.willhavetwins=0;
  newegg.objectprob=0;
  newegg.objects.clear();
  /// inherited feature from mother
  for(int k=0;k<5;k++) {
    if(rand()%2) newegg.featuregene[k][0]=featuregene[k][0];
    else newegg.featuregene[k][0]=featuregene[k][1];
    /// inherited feature from father
    if(rand()%2) newegg.featuregene[k][1]=father.featuregene[k][0];
    else newegg.featuregene[k][1]=father.featuregene[k][1];  
    /// expressed feature -- feature is a recessive gene
    if(newegg.featuregene[k][0]==newegg.featuregene[k][1] && newegg.featuregene[k][0]!="no feature") newegg.feature[k]=newegg.featuregene[k][0];
    /// feature is a dominant gene
    //if(newegg.featuregene[k][0]==newegg.featuregene[k][1] || newegg.featuregene[k][0]=="no feature") newegg.feature[k]=newegg.featuregene[k][1];
    //else if(newegg.featuregene[k][1]=="no feature") newegg.feature[k]=newegg.featuregene[k][0];
  }  
  newegg.name="egg";
  newegg.energy=1;
  newegg.age=-1;
  newegg.myaction_who="";
  newegg.myaction_what="growing";
  newegg.myaction_nturns=0;
  for(int i=0;i<heritage.size();i++) {
    newegg.heritage.push_back(heritage[i]/2+father.heritage[i]/2);
  }  
  newegg.history_children.clear();
  newegg.history_carers.clear();
  newegg.history_harassers.clear();
  newegg.history_lovers.clear();
  newegg.child_care=0;
  newegg.child_care_by_males=0;
  newegg.child_harassment=0;
  /// determine clans
  for(int cc=0;cc<4;cc++) {
    if(newegg.isclan(cc)) {
      newegg.isinclan.push_back(cc);
    }  
  }    
  if(newegg.isinclan.size()==0) newegg.isinclan.push_back(4); // rebel
//   newegg.nameindex=nameindex;  /// creature takes mother's ethnicity
  newegg.nameindex=newegg.isinclan[0];  /// creature takes clan's ethnicity
  return newegg;
}  

Creature Creature::deliver() {
  Creature newcreature;
  int chosenl=0;
  //// loop over eggs and choose one at random
  chosenl=rand()%eggs.size();
  newcreature=eggs[chosenl];
  newcreature.energy=energy_born;
  for(int cop=1;cop<eggs.size();cop++) {
    newcreature.energy+=energy_born_extra;
  }
  eggs.clear();
  pregnant=0;
  return newcreature;
}  

void Creature::kill() {
  cout << endl;
  printname();
  cout << whitebold << " is dead!" << reset << endl;
  cout << "enter key to continue" << endl;
  string keyenter;
  cin >> keyenter;
  energy=-9999;
  isdead=1;
  myaction_who=name;
  myaction_what="dead"; 
  myaction_nturns=0;
}  

Names Creature::setname(Names names,int i) {
  if(isfemale) {
    name=names.namesf[i][0];
    names.namesf[i].erase(names.namesf[i].begin());
    if(names.namesf[i].size()==0) cout << "***WARNING!!!*** no more names available after " << name << endl;
  }  
  else {
    name=names.namesm[i][0];
    names.namesm[i].erase(names.namesm[i].begin());
    if(names.namesm[i].size()==0) cout << "***WARNING!!!*** no more names available after " << name << endl;
  }    
  return names;
}  

float Creature::thisturncost(bool truecost) {
  float myagecost=0;
  float thisturncost;
  if(isold) myagecost=(age-age_old)*agecost;
  thisturncost=myagecost;
  if(truecost) {
    if(ispoisoned) {
      thisturncost+=20+rand()%10;
      ispoisoned--;
    }
    if(ispotioned) {
      thisturncost-=20+rand()%10;
      ispotioned--;
    }
  }
  else {
    if(ispoisoned) thisturncost+=30;
    if(ispotioned) thisturncost-=30;
  }  
  return thisturncost;
}  

bool Creature::isclan(int clan) {
  int clantrue=0;
  if(clan>-1 && clan<heritage.size()) {
    if(heritage[clan]>=0.5) clantrue=1;  
  }  
  return clantrue;
}

bool Creature::sameclan(Creature other) {
  bool issameclan=0;
  if(isinclan.size()==0 || other.isinclan.size()==0) issameclan=0; /// rebels are not considered a clan
  else for(int cc1=0;cc1<isinclan.size();cc1++) {
    for(int cc2=0;cc2<other.isinclan.size();cc2++) {
      if(isinclan[cc1]==other.isinclan[cc2]) issameclan=1;
    }
  }  
  return issameclan;
}

vector<int> Creature::gettotems(vector<Clan> clans) {
  vector<int> mytotems;
  int totems[2];
  totems[0]=0;
  totems[1]=-1;
  for(int ii=0;ii<isinclan.size();ii++) {
    int cc=isinclan[ii];
    totems[ii]=clans[cc].totem;
  }
  mytotems.push_back(totems[0]);
  if(totems[1]>-1) mytotems.push_back(totems[1]);
  return mytotems;
}  

int Creature::dispo(vector<Clan> clans) {
  int mybananadispo=0;
  if(isinclan.size()==0) {
    mybananadispo=clans[4].bananas;   /// rebel monkey
  }  
  else {
    for(int ii=0;ii<isinclan.size();ii++) {
      int cc=isinclan[ii];
      mybananadispo+=clans[cc].bananas; 
    }	
  }  	
  return mybananadispo;  
}  

int Creature::cost(vector<Clan> clans,int action, string war, string party) {    /// action or totem can be 1=explore, 2=love, 3=attack, 4=healing
  int mybananacost;
  if(chief>-1) mybananacost=bananacost;
  else mybananacost=bananacost_clan;
  if(action==1) mybananacost=bananafind;
  for(int ii=0;ii<gettotems(clans).size();ii++) {
    if(action>1 && gettotems(clans)[ii]!=0 && gettotems(clans)[ii]!=action) mybananacost+=bananatotemeffect;
    if(action==1 && gettotems(clans)[ii]>1) mybananacost-=bananatotemeffect;  
  }    
  if(war!="no" && action==3) mybananacost=0;
  if(party!="no" && action==2) mybananacost=0;
  return mybananacost;  
}  

int Creature::hasobject(string myobject) {    
  int objectindex=-1;
  for(int oo=0;oo<objects.size();oo++) {
    if(objects[oo]==myobject) objectindex=oo;
  }  
  return objectindex;
}  

void Creature::loseobject(string myobject) { 
  int objectindex=-1;
  if(objects.size()) {
    objectindex=hasobject(myobject);
    if(objectindex>-1) objects.erase(objects.begin()+objectindex);
    else cout << "ERROR : " << myobject << " not found!!!" << endl;
  } 
  else cout << "ERROR : " << name << " has no objects!!!" << endl;
}  

void Creature::printname() {
  if(isclan(0)) {
    if(chief>-1) cout << yellowbold << name << reset;
    else cout << yellow << name << reset;
  }  
  if(isclan(1)) {
    if(chief>-1) cout << greenbold << name << reset;
    else cout << green << name << reset;
  }           
  if(isclan(2)) {
    if(chief>-1) cout << cyanbold << name << reset;
    else cout << cyan << name << reset;
  }       
  if(isclan(3)) {
    if(chief>-1) cout << magentabold << name << reset;
    else cout << magenta << name << reset;
  }       
  if(!isclan(0) && !isclan(1) && !isclan(2) && !isclan(3)) cout << name;
}  

void Creature::print() {
  cout << endl;
  printname();
  if(!isadult) cout << " (baby)";
  if(enchanted!="no") cout << magenta << " - enchanted" << reset;
  if(ispoisoned) cout << red << " - poisoned" << reset;
  if(ispotioned) cout << green << " - rejuvenating" << reset;
  if(isarmoured) cout << green << " - armoured" << reset;
  if(extraturn) cout << yellow << " - saving for next turn" << reset;
  if(cupandballed) cout << red << " - playing cup-and-ball" << reset;
  if(willhavetwins) cout << blue << " - twins programmed" << reset;
  if(isfemale) {
    if(pregnant==-1) cout << blue << " - pregnant" << reset;
    else {
      cout << " - " << eggs.size() << " cop.";
      if(pregnant>0) cout << " - pregnancy level " << pregnant << "/100";   
    }	
  }  
  else cout << " - male"; 
  cout << " - gen. " << generation;
  cout << " - age " << trunc(age);   
  cout << " (-" << thisturncost(0) << ")";    
  cout << " - ";
  for(int k=0;k<5;k++) {
    if(feature[k]!="no feature") cout << feature[k];
  }  
  if(debug>10) cout << " - " << heritage[0] << " " <<  heritage[1] << " " << heritage[2] << " " <<  heritage[3];
  cout << " - ";
  if(energy<25) cout << red; 
  else cout << green;
  cout << energy << reset << "/" << energy_max;
  if(chief>-1) {
    for(int k=0;k<objects.size();k++) cout << " - " << objects[k];
  }  
  cout << endl;
  cout << myaction_what << " " << myaction_who << endl;
}  

bool Creature::oppositesex(Creature flirt) {
  if(isfemale && flirt.isfemale || !isfemale && !flirt.isfemale) return 0;
  else return 1;  
}  

int Creature::aiwhatwithwhom(vector<Creature> creatures, vector<Clan> clans, string war, string party) {
  int mybananadispo;  
  int totemeffect=5000;
  int p_care_sex=0;
  int p_care_clan=0;
  int p_harass=0;
  int p_search=0;
  int totp;
  vector<string> candidates_care;
  vector<string> candidates_harass;
  vector<string> children;
  candidates_care.clear();
  candidates_harass.clear();
  children.clear();
  string newaction_what=myaction_what;  
  string newaction_who=myaction_who;
  if(debug) cout << "DEBUG: " << name << " is deciding its actions. War:" << war << " ; party:" << party << endl; 
  ///// search 
  p_search=10;
  if(chief==-1) p_search+=10;
  for(int ii=0;ii<gettotems(clans).size();ii++) {
    if(gettotems(clans)[ii]==1) p_search+=totemeffect;   /// exploration totem
  }    
  ///// not enough bananas
  if(dispo(clans)<bananacost && dispo(clans)<cost(clans,2,war,party) && dispo(clans)<cost(clans,3,war,party)) {
    p_search+=10000;   //// for the moment, AI does not use objects
  }  
  ///// enough bananas for healing
  if(dispo(clans)>=cost(clans,4,war,party)) {
    ///// care for clan
    p_care_clan=10;
    if(chief==-1) p_care_clan+=10;
    if(isfemale) p_care_clan+=10;    
    if(isold) p_care_clan+=10;    
    for(int i=0;i<creatures.size();i++) {  /// increase if clan members with low energy
      if(!creatures[i].isdead && sameclan(creatures[i]) && creatures[i].energy<25) p_care_clan+=20;
    }  
    if(feature[2]=="pocket") p_care_clan+=20;  /// increase if possess pocket  
    for(int ii=0;ii<gettotems(clans).size();ii++) {
      if(gettotems(clans)[ii]==4) p_care_clan+=totemeffect;   /// healing totem
    }        
  }  
  ///// enough bananas for seducing
  if(dispo(clans)>=cost(clans,2,war,party)) {
    ///// seduce 
    p_care_sex=10;
    if(chief>-1) p_care_sex+=10;
    for(int ii=0;ii<gettotems(clans).size();ii++) {
      if(gettotems(clans)[ii]==2) p_care_sex+=totemeffect;   /// love totem
    }    
    if(!isfemale) p_care_sex+=10;
    if(energy<30) p_care_sex-=trunc(30-energy);   /// decrease mating if low energy
    if(history_children.size()==0) p_care_sex+=10;  /// increase mating if no children
    if(age<age_old) p_care_sex+=age_old-trunc(age);  /// increase if young
    if(isfemale) p_care_sex+=trunc(child_care_by_males);  /// increase if taken care of by males as a child
    /// other creature offering sex --> increase mating probability
    for(int i=0;i<creatures.size();i++) {
      if(!creatures[i].isdead && oppositesex(creatures[i]) && (creatures[i].myaction_what=="caring for" || creatures[i].myaction_what=="CARING for") && creatures[i].myaction_who==name) {
	if(!sameclan(creatures[i])) {
	  p_care_sex+=20;
	  for(int k=0;k<10;k++) candidates_care.push_back(creatures[i].name);
	}  
      }  
    }
  }  
  ///// enough bananas for attacking
  if(dispo(clans)>=cost(clans,3,war,party)) {
    ///// harass
    p_harass=10;
    for(int ii=0;ii<gettotems(clans).size();ii++) {
      if(gettotems(clans)[ii]==3) p_harass+=totemeffect;   /// war totem
    }    
    p_harass+=trunc(child_harassment);   /// increase if harassed as a child
    if(feature[1]=="claws") p_harass+=10;   /// increase if possess claws
    ///// increase chance of harass if being harassed
    for(int i=0;i<creatures.size();i++) {
      if(!creatures[i].isdead && (creatures[i].myaction_what=="attacking" || creatures[i].myaction_what=="ATTACKING") && creatures[i].myaction_who==name) {
	p_harass+=50;
	for(int k=0;k<10;k++) candidates_harass.push_back(creatures[i].name);
      }	
    }	
  }
  ////////// chose and apply action weighted on probabilities
  totp=p_search+p_care_sex+p_care_clan+p_harass;
  int random=rand()%totp;
  //cout << "!!!DEBUG!!! " << random << " ( search " << p_search << " sex " << p_care_sex << " child " << p_care_clan << " self " << p_care_self << " harass " << p_harass << " tot " << totp << ")";
  if(random<=p_care_sex) {
    /// chose best mate
    for(int i=0;i<creatures.size();i++) {
      /// check adult and opposite sex and other clan
      if(!creatures[i].isdead && creatures[i].isadult && oppositesex(creatures[i])) {
	if(!sameclan(creatures[i])) {
	  candidates_care.push_back(creatures[i].name);
	  //// favour history of good care (most relevant for females)
	  for(int k=0;k<history_carers.size();k++) {
	    if(creatures[i].name==history_carers[k]) candidates_care.push_back(creatures[i].name);
	  }
	  /// boost mane carriers
	  if(creatures[i].feature[0]=="mane") {
	    for(int k=0;k<diamondeffect;k++) {
	      candidates_care.push_back(creatures[i].name);
	    }  
	  }  
	}
      }	  
    }  	
    /// protect against impossible actions (eg all monkeys are dead)
    if(candidates_care.size()==0) candidates_care.push_back(name);    
    /// define action
    newaction_what="caring for";
    int randomguy=rand()%candidates_care.size();
    newaction_who=candidates_care[randomguy];
    mybananadispo=cost(clans,2,war,party);
  }
  else if(random<=p_care_sex+p_care_clan) {
    candidates_care.push_back(name);  // care for self
    /// chose clan member to care for
    for(int i=0;i<creatures.size();i++) {
      if(!creatures[i].isdead && sameclan(creatures[i])) {
	candidates_care.push_back(creatures[i].name);
	if(creatures[i].energy<50) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<30) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<20) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<15) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<12) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<10) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<8) candidates_care.push_back(creatures[i].name); 
	if(creatures[i].energy<6) candidates_care.push_back(creatures[i].name); 
	if(!creatures[i].isadult) candidates_care.push_back(creatures[i].name);  
	if(creatures[i].age<10) candidates_care.push_back(creatures[i].name); 
      }
    }  
    /// protect against impossible actions (eg all monkeys are dead)
    if(candidates_care.size()==0) candidates_care.push_back(name);  
    /// define action
    newaction_what="caring for";
    int randomguy=rand()%candidates_care.size();
    newaction_who=candidates_care[randomguy];
    mybananadispo=cost(clans,0,war,party);
  }
  else if(random<=p_care_sex+p_care_clan+p_harass) {
    /// chose creature to harass
    for(int i=0;i<creatures.size();i++) {
      /// do not harass children and not creatures of same clan
      if(!creatures[i].isdead && creatures[i].isadult && !sameclan(creatures[i])) {
	candidates_harass.push_back(creatures[i].name);
	/// females more likely to harass other females
	if(isfemale && creatures[i].isfemale) candidates_harass.push_back(creatures[i].name);
	/// males more likely to harass other males
	if(!isfemale && !creatures[i].isfemale) candidates_harass.push_back(creatures[i].name);
	/// more likely to harass past harassers
	for(int k=0;k<history_harassers.size();k++) {
	  if(creatures[i].name==history_harassers[k]) candidates_harass.push_back(history_harassers[k]);
	}  
      }	
    }
    /// protect against impossible actions (eg all monkeys are dead)
    if(candidates_harass.size()==0) candidates_harass.push_back(name);    
    /// define action
    newaction_what="attacking";
    int randomguy=rand()%candidates_harass.size();
    newaction_who=candidates_harass[randomguy];
    mybananadispo=cost(clans,3,war,party);
  }  
  else if(random<=p_care_sex+p_care_clan+p_harass+p_search) {
    newaction_what="searching";
    newaction_who=name;
    mybananadispo=cost(clans,1,war,party);
  }  
  /// implement new action
  if(newaction_what!=myaction_what || newaction_who!=myaction_who) myaction_nturns=0; 
  myaction_what=newaction_what;
  myaction_who=newaction_who;

  return mybananadispo;
}  

int Creature::aichoosetotem() {
  int newtotem=0;
  if(rand()%10==0) {   /// 1 chance in 10 to erect totem
    newtotem=rand()%3+1;
  }  
  return newtotem;
}  
  
///////////////////////////////////////////////

float scorebaby(Creature baby,int clan) {
  float score=0;
  if(clan<4) {   // no score for rebels
    score+=100*baby.heritage[clan];
  }  
  return score;
}  
  
float scoreadults(vector<Creature> creatures,int clan) {
  int ncreatures=creatures.size();
  float score=0;
  if(clan<4) {   // no score for rebels
    for(int i=0;i<ncreatures;i++) {  
      if(creatures[i].generation>0 && creatures[i].isadult && !creatures[i].isdead) {  /// count only living adults     
	score+=100*creatures[i].heritage[clan];
      }
    }  
  }
  return score;
}  




